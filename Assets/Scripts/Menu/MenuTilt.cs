﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class MenuTilt : MonoBehaviour {

    private float axisX;
    private float axisZ;

    SerialPort stream = new SerialPort("COM" + GlobalCOM.COMNumber.ToString(), 57600); //Set the port (com4) and the baud rate (9600, is standard on most devices)
    float[] lastRot = { 0, 0 }; //Need the last rotation to tell how far to spin the camera

	// Use this for initialization
	void Start () 
    {
        stream.ReadTimeout = 20;
        stream.Open(); //Open the Serial Stream.
	}
	// Update is called once per frame
	void Update () {

        string value = stream.ReadLine(); //Read the information
        string[] vec3 = value.Split('/'); //My arduino script returns a 3 part value (IE: 12,30,18)

        if (vec3[0] != "" && vec3[1] != "" && vec3[2] != "") //Check if all values are recieved
        {
            var tiltAroundX = float.Parse(vec3[0]) - lastRot[0] * 100;
            var tiltAroundZ = float.Parse(vec3[1]) - lastRot[1] * -100;

            //var tiltAroundX = float.Parse(vec3[0]) * -15;
            //var tiltAroundZ = float.Parse(vec3[1]) * 15;
            var target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);

            // Dampen towards the target rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 4);

            lastRot[0] = float.Parse(vec3[0]);  //Set new values to last time values for the next loop
            lastRot[1] = float.Parse(vec3[1]);

            stream.BaseStream.Flush(); //Clear the serial information so we assure we get new information.
        }
	}

    public void Reconnect()
    {
        stream.Close();
        stream = new SerialPort("COM" + GlobalCOM.COMNumber.ToString(), 57600);
        stream.Open();
    }
}
