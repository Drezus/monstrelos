﻿using UnityEngine;
using System.Collections;

public class MonstreloBolinha : MonoBehaviour {

    private Vector3 startPos;

	// Use this for initialization
	void Start () {

        Time.timeScale = 2;
        startPos = transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter (Collider hit)
    {
        if (hit.tag == "buraco")
        {
            renderer.active = false;
            GameObject.Find("BaseLabirinto").GetComponent<LabrintoTilt>().changeControl(false);
            Invoke("Reappear", 1);
        }

        if (hit.tag == "fimMaze")
        {
            Time.timeScale = 1;
            Application.LoadLevel(0);
        }
    }

    void Reappear()
    {
        transform.position = startPos;
        renderer.active = true;
        GameObject.Find("BaseLabirinto").GetComponent<LabrintoTilt>().changeControl(true);
    }
}
