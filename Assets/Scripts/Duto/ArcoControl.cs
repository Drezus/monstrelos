﻿using UnityEngine;
using System.Collections;

public class ArcoControl : MonoBehaviour {

    public GameObject arco;

    public int arcNum = 0;

    public GUIText arcText;

    public void Start()
    {
        SpawnArco();
    }

    void SpawnArco()
    {
        arco = Instantiate(arco, new Vector3(transform.position.x, Random.Range(244, 249), transform.position.z), arco.transform.rotation) as GameObject;
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.tag == "arco")
        {
            arcNum++;
            Destroy(hit.gameObject);
            SpawnArco();
        }
    }

    public void Update()
    {
        arcText.text = "Arcos: " + arcNum + " de 20";

        if (arcNum >= 20)
        {
            Application.LoadLevel(0);
        }
    }
}
