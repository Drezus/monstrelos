﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class MonstreloControl : MonoBehaviour {

    private float force;
    public ParticleEmitter vento;
    public GameObject monstreloModel;

    SerialPort stream = new SerialPort("COM" + GlobalCOM.COMNumber.ToString(), 57600);

	// Use this for initialization
	void Start () 
    {
        force = 10f;

        vento.localVelocity = new Vector3(0, force, 0);
        monstreloModel.rigidbody.AddRelativeTorque(Random.Range(0, 100), Random.Range(0, 100), Random.Range(0, 100));

        stream.ReadTimeout = 20;
        stream.Open(); //Open the Serial Stream.
	}
	
	// Update is called once per frame
	void Update () {

        monstreloModel.transform.position = transform.position;

        string value = stream.ReadLine(); //Read the information
        string[] vec3 = value.Split('/'); //My arduino script returns a 3 part value (IE: 12,30,18)

        if (vec3[3] != "") //Check if all values are recieved
        {
            int forceVal = int.Parse(vec3[3]);

            if (forceVal > 500)
            {
                monstreloModel.rigidbody.angularVelocity = Vector3.zero;
                monstreloModel.rigidbody.AddRelativeTorque(Random.Range(0, 100), Random.Range(0, 100), Random.Range(0, 100));
            }

            if (forceVal > 0)
            {
                rigidbody.AddForce(0, force + (forceVal / 50), 0);
                vento.localVelocity = new Vector3(0, force * 1.5f, 0);
            }
            else
            {
                rigidbody.AddForce(0, (force + (forceVal / 50)) / 1.4f, 0);
                vento.localVelocity = new Vector3(0, force - 2, 0);
            }

            stream.BaseStream.Flush(); //Clear the serial information so we assure we get new information.
        }    
	}

    void OnTriggerStay(Collider hit)
    {
        if (hit.tag == "forceZone")
        {
            rigidbody.AddForce(0, 4, 0);
        }
    }
}
