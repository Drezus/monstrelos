﻿using UnityEngine;
using System.Collections;

public class EscToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Application.loadedLevel == 0)
            {
                Application.Quit();
            }
            else
            {
                Time.timeScale = 1;
                Application.LoadLevel(0); 
            }
        }
	
	}
}
