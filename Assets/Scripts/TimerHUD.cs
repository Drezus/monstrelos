﻿using UnityEngine;
using System.Collections;

public class TimerHUD : MonoBehaviour {

    private float timer = 0;

    public void FixedUpdate()
    {
        if (Application.loadedLevelName == "labirinto")
        {
            timer += Time.deltaTime / 2;
        }
        else
        {
            timer += Time.deltaTime;
        }

        int minutes = (int)(timer / 60);
        int seconds = (int)(timer % 60);

        string s = string.Format("{0:00}:{1:00}", minutes, seconds);

        guiText.text = s;
    }
}
