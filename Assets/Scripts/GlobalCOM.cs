﻿using UnityEngine;
using System.Collections;

public class GlobalCOM : MonoBehaviour {

    public static int COMNumber;
    public GameObject monstrelo;

	// Use this for initialization
	void Awake () 
    {

	}

    void OnGUI()
    {
        GUI.Label(new Rect(50, 600, 600, 30), "Porta COM:");
        COMNumber = int.Parse(GUI.TextField(new Rect(150, 600, 60, 20), COMNumber.ToString()));
        if (GUI.Button(new Rect(50, 630, 100, 30), "Reconectar"))
        {
            monstrelo.GetComponent<MenuTilt>().Reconnect();
        }
    }
}
