﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class MonstreloJump : MonoBehaviour {

    SerialPort stream = new SerialPort("COM" + GlobalCOM.COMNumber.ToString(), 57600); //Set the port (com4) and the baud rate (9600, is standard on most devices)
    GameObject[] aPlats;
    bool collidersActive = true;

    // Use this for initialization
    void Start()
    {
        aPlats = GameObject.FindGameObjectsWithTag("jumpPlat");

        stream.ReadTimeout = 20;
        stream.Open(); //Open the Serial Stream.
    }

	// Update is called once per frame
	void Update () 
    {
        string value = stream.ReadLine(); //Read the information
        string[] vec3 = value.Split('/'); //My arduino script returns a 3 part value (IE: 12,30,18)

        if (vec3[1] != "") //Check if all values are recieved
        {
            rigidbody.AddForce(Vector3.right * float.Parse(vec3[1]));

            stream.BaseStream.Flush(); //Clear the serial information so we assure we get new information.
        }

        if (rigidbody.velocity.y < 0 &&  !collidersActive)
        {
            ActivateColliders();
        }
	}

    void LateUpdate()
    {
        GameObject.Find("Main Camera").transform.position = new Vector3(0, GameObject.Find("Main Camera").transform.position.y, GameObject.Find("Main Camera").transform.position.z);
    }

    void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.tag == "jumpPlat")
        {
            RemoveColliders();
            rigidbody.AddForce(Vector3.up * 30);
        }
    }

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == "fimJump")
        {
            Application.LoadLevel(0);
        }
    }



    void RemoveColliders()
    {
        foreach (GameObject plat in aPlats)
        {
            plat.GetComponent<BoxCollider>().enabled = false;
        }

        collidersActive = false;
    }

    void ActivateColliders()
    {
        foreach (GameObject plat in aPlats)
        {
            plat.GetComponent<BoxCollider>().enabled = true;
        }

        collidersActive = true;
    }
}
